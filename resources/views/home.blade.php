@extends('layouts.master')
@section('title')
Home Page
@endsection
@section('content')
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="col-md-4"><a href="{{ route('greed') }}">Greet</a></div>
        <div class="col-md-4"><a href="{{ route('hug') }}">Hug</a></div>
        <div class="col-md-4"><a href="{{ route('kiss') }}">Kiss</a></div>

        <form method="post" action="benice" class="form-group">
            <label for="select-action">I Want to</label>
            <select name="action" id="select-action">
                <option value="Greet">Greet</option>
                <option value="Kiss">Kiss</option>
                <option value="Hug">Hug</option>
            </select>
            <input type="text" placeholder="Enter your name" class="input-medium" name="name">
             <button type="submit" class="btn btn-success">Do a nice action</button>
            <input type="hidden" value="{{ Session::token() }}" name="_token">
        </form>
    </div>
    <div class="col-md-3"></div>

@endsection

