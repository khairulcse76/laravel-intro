@extends('layouts.master')
@section('title')Be nice @endsection
@section('content')
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <a href="{{ route('home') }}">Back</a>
        <h1>I Want To {{ $action }} You {{ $name }}</h1>
    </div>
    <div class="col-md-2"></div>
@endsection