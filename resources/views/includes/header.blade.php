<div class="row">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="col-md-2">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ route('home') }}">A New Blog</a>
                </div>
            </div>
            <div class="col-md-6">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>