<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/greed/{name?}', function ($name=null) {
    return view('action.greed', ['name' =>$name]);
})->name('greed');
Route::get('/hug', function () {
    return view('action.hug');
})->name('hug');;
Route::get('/kiss', function () {
    return view('action.kiss');
})->name('kiss');


Route::post('/benice',[
    'uses'=>'NiceActionController@postNiceAction',
    'as'=>'nice',
]);