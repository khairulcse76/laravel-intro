<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NiceActionController extends Controller
{
    public function postNiceAction(Request $request){
        if (isset($request['action']) && ($request['name'])){
            if (strlen($request['name'])>0){
                return view('action.nice', ['action' => $request['action'],'name'=>$this->transfareNametoUpper($request['name'])]);
            }
            return redirect()->back();
        }
        return redirect()->back();
    }

    private function transfareNametoUpper($name){

        return $prefix=strtoupper($name);
    }

    private function transfareNametoLowar($name){

        return $prefix=strtolower($name);
    }

}
